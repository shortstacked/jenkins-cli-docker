FROM openjdk:8-jdk-alpine
LABEL maintainer="declancarroll@gmail.com"
COPY ./jenkins-cli.jar jenkins-cli.jar
ENTRYPOINT [ "java", "-jar", "/jenkins-cli.jar", "-webSocket"]